fun main() {
    val numArray = doubleArrayOf(45.3, 67.5, -45.6, 20.34, 33.0, 45.6)
    var jami = 0.0
    var c = 0

    for (number in numArray) {
        if(c % 2 == 0){
            jami += number
        }
        c += 1
    }

    val average = jami / numArray.size
    println("The average is: %.2f".format(average))
}