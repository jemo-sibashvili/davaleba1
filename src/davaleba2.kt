fun main(){
    val i = p("ana ana  ana")
    print(i)
}

fun p(x: String): Boolean {
    val a = x.lowercase().replace(" ", "")
    return a == a.reversed()
}